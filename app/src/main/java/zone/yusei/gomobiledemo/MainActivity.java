package zone.yusei.gomobiledemo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import hello.Hello;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 调用so中方法
        final String greeting = Hello.greeting("Tyler Hou");
        Toast.makeText(this, greeting, Toast.LENGTH_LONG).show();
        Log.i("HELLO", greeting);
    }
}